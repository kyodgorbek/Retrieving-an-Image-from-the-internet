# Retrieving-an-Image-from-the-internet
Retrieving an Image from on the internet on J2ME
import java.io.*;
import java.io.*;
import javax.microedition.lcdui.*;
import javax.microedition.midlet*;

public class ImageLoader
	 extends MIDlet
	 implements CommandListener, Runnable {
    private Display mDisplay;
    private Form mForm;

    public ImageLoader() {
	mForm = new Form("Connecting...");
	mForm.addCommand(new Command)("Exit", Command.EXIT, 0));
	mForm.setCommandListener(this);
    }
    
    public void startApp() {
	 if(mDisplay == null) mDisplay = Display.getDisplay(this);
	 mDisplay.setCurrent(mForm);
	
	// Do network loading in a separate thread.
	Thread t = new Thread(this);
	t.start();
     }
     
      public void pauseApp(){}
	
      public void destroyApp(boolean unconditional) {}
      
      public void commandAction(Command c, Displayable s){
	 if (c.getCommandType() == Command.EXIT)
	   notifyDestroyed();
 }
      public void run(){
	HttpConnection hc = null;
	DataInputStream in = null;
	
	try {
	 String url = getAppProperty("ImageLoader-URL");
	 hc = (HttpConnection)Connector.open(url);
	 int length = (int)hc.getLength();
	 byte[] data = null;
	 if (length != -1) {
	     data new byte[length];
	     in = new DataInputStream(hc.openInputStream());
	     in.readFully(data);
	  import javax.microedition.midlet.*;
import javax.microedition.lcdui*;
import javax.microedition.pim*;
import java.io*;
import java.util*;
import javax.mcroedition.lcdui.List;

public class PIMMidlet
extends MIDlet
implements CommandListener {
	private ContactList contList = null;
	private Enumaration contacts = null;

private Form mForm;
private List mNameList;
private Command mExitCommand;
public PIMMIDlet() {
	try {
	verifyPIMSupport();
	PIM pimInst = PIM.getInstance();
	contList = (ContactList)
	pimInst.openPIMList(PIM.CONTACT_LIST, PIM.RAD_ONLY);
	contacts = contList.items();
    }
    catch (Exception ex) {
	mForm = new Form("Exception");
	mForm.append(new StringItem(null, ex.toString()));
	mExitCommand = new Command("Exit", Command.EXIT, 0);
	mForm.addCommand(mExitCommand);
	mForm.setCommandListener(this);
	return;
    }
    
    if (contacts = null)
     return;
  mNameList = new List("List of contacts", ListEXCLUSIVE);
  while (contacts.hasMoreElements()) {
	  Contact tCont = (Contact) contacts.nextElement();
	  int [] fids = tCont.getFields();
	  String [] nameValues = tCont.getStringArray( Contact.NAME, 0);
	  String firstName = nameValues[Contact.NAME_GIVEN];
	  String lastName = nameValues[Contact.NAME_FAMILY];
	  mNameList.append(lastName + ", " " + firstName, null);
     }
     
     
     mExitCommand = new Command("Exit", Command.EXIT, 0);
     mNameList.addCommand(mExitCommand);
     mNameList.setCommandListener(this);
   }
   
   public void startApp() {
	Display.getDisplay(this).setCurrent(mNameList);
   }
   
   public void pauseApp() {}
   public void destroyApp(boolean flg) {
   }
   public void commandAction(Command c, Displayable s) {
	if (c == mExitCommand) {
	    destroyApp(true);
	    notifyDestroyed();
   public void run() {
	HttpConnection hc = null;
	DataInputStream in = null;
	
	try{
	 String url = getAppProperty("ImageLoader-URL");
	 hc = (HttpConnection)Connector.open(url);
	 int length (int)hc.getLength();
	 byte[] data = null;
	 if (length != -1){
	     data = new byte[length];
	     in = new DataInputStream(hc.openInputStream());
	     in.readFully(data);
	 }
	 else {
	  // If content length is not given, read in chunks.
	  int chunkSize = 512;
	  int index = 0;
	  int readLength = 0;
	  in new DataInputStream(hc.openInputStream());
	  data = new byte[chunkSize];
	  do{
	   if (data.length < index + chunkSize) {
	      byte[] newData = new byte[index + chunkSize];
	      System.arraycopy(data, 0, NewData, 0, data.length);
	      data = newData;
	   }
	   readLength = in.read(data, 0, newData, 0, data.length);
	   index = += readLength;
   } while (readLength == chunkSize);
      length = index;
   }
   Image image = Image.createImage(data, 0, length);
   ImageItem ImageItem = new ImageItem(null, image, 0, null);
   mForm.append(imageItem);
   mForm.setTitle("Done.");
}
catch (IOException ioe) {
    StringItem stringItem = new StringItem(null, ioe.toString());
    mForm.append(stringItem);
    mForm.setTitle("Done.");
}
  finally {
	try {
		if (in != null) in.close();
		if (hc != null) hc.close();
	   }
           catch (IOException ioe) {}
	}
     }	   
}
